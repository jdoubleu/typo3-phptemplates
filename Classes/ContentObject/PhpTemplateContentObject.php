<?php
namespace Jdoubleu\Phptemplates\ContentObject;

use Jdoubleu\Phptemplates\Rendering\DefaultView;
use Jdoubleu\Phptemplates\Rendering\Exception\RenderingException;
use Jdoubleu\Phptemplates\Rendering\Exception\SecurityException;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\ContentObject\AbstractContentObject;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;

class PhpTemplateContentObject extends AbstractContentObject
{
    /**
     * @var DefaultView the default view used to render templates
     */
    protected $view;

    /**
     * PhpTemplateContentObject constructor.
     * @param ContentObjectRenderer $cObj
     */
    public function __construct(ContentObjectRenderer $cObj)
    {
        parent::__construct($cObj);
    }

    /**
     * Renders the content object.
     *
     * @param array $conf
     * @return string
     * @throws RenderingException
     * @throws SecurityException
     */
    public function render($conf = [])
    {
        $parentView = $this->view;

        if (!is_array($conf)) {
            $conf = [];
        }

        // resolve variables
        $variables = $this->getContentObjectVariables($conf);

        // create view
        $this->view = new DefaultView(
            $this->getTemplateName($conf),
            $this->getTemplatePaths($conf),
            $variables,
            $this->cObj,
            $conf,
            $GLOBALS['TYPO3_CONF_VARS']['FE']['debug'] ?? false
        );

        // render the actual content
        $content = $this->view->render();

        $this->view = $parentView;

        return $content;
    }

    /**
     * Sets the template paths in the view
     * @param array $conf cObj configuration
     * @return array template paths
     */
    protected function getTemplatePaths($conf)
    {
        if (!empty($conf['templateRootPaths.']) && is_array($conf['templateRootPaths.'])) {
            $paths = array_values(array_filter($conf['templateRootPaths.']));
            return array_map([ GeneralUtility::class, 'getFileAbsFileName'], $paths);
        }

        return [];
    }

    /**
     * Sets the template name to render
     * @param array $conf cObj configuration
     * @return string template name
     */
    protected function getTemplateName($conf)
    {
        if (!empty($conf['templateName']) || !empty($conf['templateName.'])) {
            return isset($conf['templateName.'])
                ? $this->cObj->stdWrap($conf['templateName'] ?? '', $conf['templateName.'])
                : $conf['templateName'];
        }

        return 'Default';
    }

    /**
     * Compile rendered content objects in variables array ready to assign to the view
     *
     * @param array $conf Configuration array
     * @return array the variables to be assigned
     * @throws \InvalidArgumentException
     */
    protected function getContentObjectVariables(array $conf)
    {
        $variables = [];
        $reservedVariables = ['data', 'current'];
        // Accumulate the variables to be process and loop them through cObjGetSingle
        $variablesToProcess = (array)($conf['variables.'] ?? []);
        foreach ($variablesToProcess as $variableName => $cObjType) {
            if (is_array($cObjType)) {
                continue;
            }
            if (!in_array($variableName, $reservedVariables)) {
                $variables[$variableName] = $this->cObj->cObjGetSingle($cObjType, $variablesToProcess[$variableName . '.']);
            } else {
                throw new \InvalidArgumentException(
                    'Cannot use reserved name "' . $variableName . '" as variable name in FLUIDTEMPLATE.',
                    1288095720
                );
            }
        }
        $variables['data'] = $this->cObj->data;
        $variables['current'] = $this->cObj->data[$this->cObj->currentValKey ?? null] ?? null;
        return $variables;
    }
}
