<?php
namespace Jdoubleu\Phptemplates\Rendering\Exception;

/**
 * Class HelperException
 * A sub exception which should be used in helper functions
 */
class HelperException extends RenderingException
{
}
