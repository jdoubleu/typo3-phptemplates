<?php
namespace Jdoubleu\Phptemplates\Rendering;

/**
 * Class OutputCaptureStack
 * A stack which captures the followed content
 */
class OutputCaptureStack
{
    /**
     * @var array call argument stack
     */
    protected $stack = [];

    /**
     * Pushes the callback to the stack and starts capturing the output
     * @param callable $callback
     * @param array $callbackArguments initial arguments passed to the callback when finished
     */
    public function push(callable $callback, array $callbackArguments = [])
    {
        array_push($this->stack, [
            $callback,
            $callbackArguments
        ]);
        ob_start();
    }

    /**
     * Stops capturing the output and invokes the last callback from stack
     * @param null $endCallbackArguments final arguments passed to the callback
     * @return mixed return value from the callback
     */
    public function pop($endCallbackArguments = null)
    {
        list($callback, $callbackArguments) = array_pop($this->stack);
        $content = ob_get_clean();

        return $callback($content, $endCallbackArguments, $callbackArguments);
    }
}
