<?php
namespace Jdoubleu\Phptemplates\Rendering;

use Jdoubleu\Phptemplates\Rendering\Exception\RenderingException;
use Jdoubleu\Phptemplates\Rendering\Exception\SecurityException;
use Jdoubleu\Phptemplates\Rendering\Helper\Debug;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Web\Request as WebRequest;
use TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;

require_once 'Helper/Common.php';
require_once 'Helper/TYPO3.php';

/**
 * Class DefaultView
 * This view is responsible for rendering the PHP templates
 */
class DefaultView
{
    /**
     * @var RenderingContext base rendering context
     */
    public $renderingContext;

    /**
     * @var bool
     */
    public $debug;

    /**
     * DefaultView constructor.
     * @param string $templateName
     * @param array $templatePaths
     * @param array $variables
     * @param ContentObjectRenderer $cOjb
     * @param array $cObjConfiguration
     * @param bool $debug
     * @throws RenderingException
     * @throws SecurityException
     */
    public function __construct(
        string $templateName,
        array $templatePaths,
        array $variables,
        ContentObjectRenderer $cOjb,
        array $cObjConfiguration,
        bool $debug = false
    ) {
        $this->renderingContext = RenderingContext::getInstance($this);
        $this->renderingContext->templateName = $templateName;
        $this->validateTemplatePaths($templatePaths);
        $this->renderingContext->templatePaths = $templatePaths;
        $this->renderingContext->variables = $variables;
        $this->renderingContext->contentObjectRenderer = $cOjb;
        $this->renderingContext->contentObjectConfiguration = $cObjConfiguration;
        $this->renderingContext->debug = $this->debug = $debug;

        $objectManager = GeneralUtility::makeInstance(ObjectManager::class);

        /** @var WebRequest $request */
        $request = $objectManager->get(WebRequest::class);
        $request->setRequestUri(GeneralUtility::getIndpEnv('TYPO3_REQUEST_URL'));
        $request->setBaseUri(GeneralUtility::getIndpEnv('TYPO3_SITE_URL'));
        /** @var UriBuilder $uriBuilder */
        $uriBuilder = $objectManager->get(UriBuilder::class);
        $uriBuilder->setRequest($request);
        $this->renderingContext->currentRequest = $request;
        $this->renderingContext->uriBuilder = $uriBuilder;
    }

    /**
     * Renders the template
     * @return string output
     * @throws RenderingException
     */
    public function render()
    {
        return $this->getTemplate($this->renderingContext->templateName);
    }

    /**
     * Renders a template
     * @param string $templateName
     * @param array $arguments
     * @throws RenderingException $return void echos the output
     */
    public function renderTemplate(string $templateName, array $arguments = [])
    {
        $templateFile = $this->resolveTemplateFile($templateName);
        $renderingClosure = $this->createIncludeClosure();

        $renderingClosure($templateFile, $arguments);
    }

    /**
     * Renders a template and returns its output
     * @param string $templateName
     * @param array $arguments
     * @return false|string rendered template output
     * @throws RenderingException
     */
    public function getTemplate(string $templateName, array $arguments = [])
    {
        ob_start();
        $this->renderTemplate($templateName, $arguments);
        return ob_get_clean();
    }

    /**
     * Creates a closure which renders/includes the php file
     * @return \Closure rendering closure
     */
    protected function createIncludeClosure()
    {
        return function(string $templateFile, array $arguments = []) {
            $this->renderingContext->createTemplateContext($templateFile, $arguments);

            require $templateFile;

            $lastTemplateContext = $this->renderingContext->popTemplateContext();

            if ($this->debug) {
                Debug::debug_template($this->renderingContext, $lastTemplateContext);
            }
        };
    }

    /**
     * Resolves the template file to render
     * @param string $templateName
     * @return string path to the template
     * @throws RenderingException if the template could not be resolved
     */
    protected function resolveTemplateFile(string $templateName)
    {
        $templatePaths = $this->renderingContext->templatePaths;

        $templateFiles = array_map(function ($path) use ($templateName) {
            return $path . '/' . $templateName . '.php';
        }, $templatePaths);

        foreach ($templateFiles as $file) {
            if (file_exists($file)) {
                return $file;
            }
        }

        throw new RenderingException(
            "Could not resolve template \"${templateName}\". Tried \"" . implode(", ", $templateFiles) . "\".",
            1558526279
        );
    }

    /**
     * Checks if the template paths are valid
     * @param array $paths
     * @throws RenderingException if a template does not exist
     * @throws SecurityException if a template path points to a remote resource
     */
    protected function validateTemplatePaths(array $paths)
    {
        foreach ($paths as $path) {
            if (filter_var($path, FILTER_VALIDATE_URL)) {
                throw new SecurityException(
                    "The template path \"{$path}\" is prohibited because external resources a not allowed!",
                    1558525654
                );
            }

            if (!file_exists($path)) {
                throw new RenderingException(
                    "The given template path \"{$path}\" does not exist.",
                    1558525854
                );
            }
        }
    }
}
