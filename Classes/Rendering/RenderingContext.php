<?php

namespace Jdoubleu\Phptemplates\Rendering;

use TYPO3\CMS\Extbase\Mvc\Web\Request as WebRequest;
use TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;

class RenderingContext implements \JsonSerializable
{
    protected static $instance = null;

    /**
     * @var DefaultView
     */
    public $view;

    /**
     * @var bool
     */
    public $debug;

    /**
     * @var string[]
     */
    public $templatePaths = [];

    /**
     * @var string name of the template to render
     */
    public $templateName = 'Default';

    /**
     * @var array static variables passed to every template
     */
    public $variables = [];

    /**
     * @var OutputCaptureStack
     */
    public $outputCaptureStack;

    /**
     * @var TemplateContext[]
     */
    public $templateContextStack = [];

    /**
     * @var TemplateContext
     */
    public $currentTemplateContext;

    /**
     * @var ContentObjectRenderer
     */
    public $contentObjectRenderer;

    /**
     * @var array Original content object configuration
     */
    public $contentObjectConfiguration;

    /**
     * @var WebRequest
     */
    public $currentRequest;

    /**
     * @var UriBuilder
     */
    public $uriBuilder;

    /**
     * @param DefaultView $view
     * @return RenderingContext
     */
    public static function getInstance(?DefaultView $view)
    {
        if (static::$instance == null) {
            if ($view == null) {
                throw new \RuntimeException(sprintf(
                    'Missing %s when initializing %s Singleton',
                    DefaultView::class, self::class
                ));
            }

            static::$instance = new self($view);
        }

        return static::$instance;
    }

    /**
     * Clear current Singleton instance.
     */
    public static function clearInstance()
    {
        static::$instance = null;
    }

    /**
     * RenderingContext constructor.
     * @param DefaultView $view
     */
    protected function __construct(DefaultView $view)
    {
        $this->view = $view;
        $this->outputCaptureStack = new OutputCaptureStack();
    }

    /**
     * RenderingContext destructor.
     */
    public function __destruct()
    {
        static::clearInstance();
    }

    /**
     * Creates a new TemplateContext and adds the last to the stack
     * @param string $templateFile
     * @param array $templateArguments
     * @return TemplateContext
     */
    public function createTemplateContext(string $templateFile, array $templateArguments)
    {
        array_push($this->templateContextStack, $this->currentTemplateContext);
        $this->currentTemplateContext = new TemplateContext($this, $templateFile, $templateArguments);

        return $this->currentTemplateContext;
    }

    /**
     * Pops the last TemplateContext from the stack and sets it as the current
     * @return TemplateContext
     */
    public function popTemplateContext()
    {
        return $this->currentTemplateContext = array_pop($this->templateContextStack);
    }

    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        $data = (array) $this;
        unset($data['view']);
        unset($data['contentObjectRenderer']);
        unset($data['uriBuilder']);
        unset($data['currentRequest']);

        return $data;
    }
}
