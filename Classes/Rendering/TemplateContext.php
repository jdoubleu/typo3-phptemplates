<?php
namespace Jdoubleu\Phptemplates\Rendering;

class TemplateContext implements \JsonSerializable
{
    /**
     * @var RenderingContext parent rendering context
     */
    public $renderingContext;

    /**
     * @var string path to the template file
     */
    public $templateFile;

    /**
     * @var array template arguments
     */
    public $arguments = [];

    /**
     * TemplateContext constructor.
     * @param RenderingContext $renderingContext
     * @param string $templateFile
     * @param array $templateArguments
     */
    public function __construct(RenderingContext $renderingContext, string $templateFile, array $templateArguments = [])
    {
        $this->renderingContext = $renderingContext;
        $this->templateFile = $templateFile;
        $this->arguments = $templateArguments;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return [
            'renderingContext' => spl_object_hash($this->renderingContext),
            'templateFile' => $this->templateFile,
            'arguments' => $this->arguments
        ];
    }
}
