<?php
namespace Jdoubleu\Phptemplates\Rendering\Helper\TYPO3;



use Jdoubleu\Phptemplates\Rendering\Exception\HelperException;
use Jdoubleu\Phptemplates\Rendering\Exception\RenderingException;
use Jdoubleu\Phptemplates\Rendering\Helper\Common;
use TYPO3\CMS\Core\TypoScript\Parser\TypoScriptParser;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\PathUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Reflection\ObjectAccess;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\ContentObject\DataProcessorInterface;
use TYPO3\CMS\Frontend\DataProcessing\MenuProcessor;
use TYPO3\CMS\Frontend\Service\TypoLinkCodecService;

/**
 * Provides TYPO3 helper functions, accessible in templates
 */
final class TYPO3 {
    /**
     * @param string $key
     * @param array $arguments
     * @param string|null $lang
     * @param string $default
     * @return string|null
     */
    public static function get_translate(string $key, array $arguments = [], string $lang = null, string $default = '')
    {
        $value = LocalizationUtility::translate($key, null, $arguments, $lang, null);

        if ($value === null && $default) {
            $value = $default;
        }

        return $value;
    }

    /**
     * @param string $key
     * @param array $arguments
     * @param string|null $lang
     * @param string $default
     * @return string|null
     */
    public static function translate(string $key, array $arguments = [], string $lang = null, string $default = '')
    {
        echo static::get_translate($key, $arguments, $lang, $default);
    }

    /**
     * @param string $parameter
     * @param string $additionalParams
     * @param bool $useCacheHash
     * @param bool $addQueryString
     * @param string $addQueryStringMethod
     * @param string $addQueryStringExclude
     * @param bool $absolute
     * @see \TYPO3\CMS\Fluid\ViewHelpers\Uri\TypolinkViewHelper
     * @return string
     */
    public static function get_typolink(
        string $parameter,
        string $additionalParams = '',
        bool $useCacheHash = false,
        bool $addQueryString = false,
        string $addQueryStringMethod = 'GET',
        string $addQueryStringExclude = '',
        bool $absolute = false
    )
    {
        $content = '';
        if ($parameter) {
            function createTypolinkParameterFromArguments($parameter, $additionalParameters = '')
            {
                $typoLinkCodec = GeneralUtility::makeInstance(TypoLinkCodecService::class);
                $typolinkConfiguration = $typoLinkCodec->decode($parameter);

                // Combine additionalParams
                if ($additionalParameters) {
                    $typolinkConfiguration['additionalParams'] .= $additionalParameters;
                }

                return $typoLinkCodec->encode($typolinkConfiguration);
            }

            $contentObject = GeneralUtility::makeInstance(ContentObjectRenderer::class);
            $content = $contentObject->typoLink_URL(
                [
                    'parameter' => createTypolinkParameterFromArguments($parameter, $additionalParams),
                    'useCacheHash' => $useCacheHash,
                    'addQueryString' => $addQueryString,
                    'addQueryString.' => [
                        'method' => $addQueryStringMethod,
                        'exclude' => $addQueryStringExclude
                    ],
                    'forceAbsoluteUrl' => $absolute
                ]
            );
        }

        return $content;
    }

    /**
     * @param string $path
     * @param string $extensionName
     * @param bool $absolute
     * @see \TYPO3\CMS\Fluid\ViewHelpers\Uri\ResourceViewHelper
     * @return string
     */
    public static function get_resourcepath(string $path, string $extensionName = null, bool $absolute = false)
    {
        $uri = 'EXT:' . GeneralUtility::camelCaseToLowerCaseUnderscored($extensionName) . '/Resources/Public/' . $path;
        $uri = GeneralUtility::getFileAbsFileName($uri);
        if ($absolute === false && $uri !== false) {
            $uri = PathUtility::getAbsoluteWebPath($uri);
        }
        if ($absolute === true) {
            $uri = PathUtility::stripPathSitePrefix($uri);
            $uri = Common::context()->currentRequest->getBaseUri() . $uri;
        }

        return $uri;
    }

    /**
     * @param int $pageUid
     * @param array $additionalParams
     * @param int $pageType
     * @param bool $noCache
     * @param bool $noCacheHash
     * @param string $section
     * @param bool $linkAccessRestrictedPages
     * @param bool $absolute
     * @param bool $addQueryString
     * @param array $argumentsToBeExcludedFromQueryString
     * @param string $addQueryStringMethod
     * @see \TYPO3\CMS\Fluid\ViewHelpers\Uri\PageViewHelper
     * @return string
     */
    public static function get_pagelink(
        int $pageUid,
        array $additionalParams = [],
        int $pageType = 0,
        bool $noCache = false,
        bool $noCacheHash = false,
        string $section = '',
        bool $linkAccessRestrictedPages = false,
        bool $absolute = false,
        bool $addQueryString = false,
        array $argumentsToBeExcludedFromQueryString = [],
        string $addQueryStringMethod = 'GET'
    )
    {
        $uriBuilder = Common::context()->uriBuilder;
        return $uriBuilder
            ->setTargetPageUid($pageUid)
            ->setTargetPageType($pageType)
            ->setNoCache($noCache)
            ->setUseCacheHash(!$noCacheHash)
            ->setSection($section)
            ->setLinkAccessRestrictedPages($linkAccessRestrictedPages)
            ->setArguments($additionalParams)
            ->setCreateAbsoluteUri($absolute)
            ->setAddQueryString($addQueryString)
            ->setArgumentsToBeExcludedFromQueryString($argumentsToBeExcludedFromQueryString)
            ->setAddQueryStringMethod($addQueryStringMethod)
            ->build();
    }

    /**
     * Resolves the TypoScript at the given object path
     *
     * @param string $typoscriptObjectPath
     * @return array
     * @throws HelperException
     */
    public static function typoscript_at(string $typoscriptObjectPath)
    {
        $pathSegments = GeneralUtility::trimExplode('.', $typoscriptObjectPath);
        $lastSegment = array_pop($pathSegments);

        $configurationManager = GeneralUtility::makeInstance(ObjectManager::class)->get(ConfigurationManagerInterface::class);
        $setup = $configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT);

        foreach ($pathSegments as $segment) {
            if (!array_key_exists($segment . '.', $setup)) {
                throw new HelperException(
                    'TypoScript object path "' . $typoscriptObjectPath . '" does not exist',
                    1565259178
                );
            }
            $setup = $setup[$segment . '.'];
        }

        return [$setup[$lastSegment] ?? null, $setup[$lastSegment . '.'] ?? []];
    }

    /**
     * Parses inline TypoScript string to PHP Array
     *
     * @param string $typoscript
     * @return array
     */
    public static function parse_typoscript(string $typoscript)
    {
        /** @var TypoScriptParser $typoscriptParser */
        $typoscriptParser = GeneralUtility::makeInstance(TypoScriptParser::class);
        $typoscriptParser->parse($typoscript);

        // TODO: handle typoscript caching
        return $typoscriptParser->setup;
    }

    /**
     * @param string $typoscriptObjectPath
     * @param $data
     * @param string $currentValueKey
     * @param string $table
     * @return mixed
     * @throws HelperException
     *
     * @see \TYPO3\CMS\Fluid\ViewHelpers\CObjectViewHelper
     */
    public static function get_typoscript(string $typoscriptObjectPath, $data, string $currentValueKey = null, string $table = '')
    {
        $contentObjectRenderer = GeneralUtility::makeInstance(
            ContentObjectRenderer::class,
            $GLOBALS['TSFE']
        );

        $currentValue = null;
        if (is_object($data)) {
            $data = ObjectAccess::getGettableProperties($data);
        } elseif (is_string($data) || is_numeric($data)) {
            $currentValue = (string)$data;
            $data = [$data];
        }

        $contentObjectRenderer->start($data, $table);
        if ($currentValue !== null) {
            $contentObjectRenderer->setCurrentVal($currentValue);
        } elseif ($currentValueKey !== null && isset($data[$currentValueKey])) {
            $contentObjectRenderer->setCurrentVal($data[$currentValueKey]);
        }

        list($setup, $conf) = static::typoscript_at($typoscriptObjectPath);
        if ($setup === null) {
            throw new HelperException(
                'No Content Object definition found at TypoScript object path "' . $typoscriptObjectPath . '"',
                1565259217
            );
        }

        $content = $contentObjectRenderer->cObjGetSingle($setup, $conf);

        return $content;
    }

    /**
     * @param string $typoscriptObjectPath
     * @param $data
     * @param string|null $currentValueKey
     * @param string $table
     * @throws HelperException
     */
    public static function typoscript(string $typoscriptObjectPath, $data, string $currentValueKey = null, string $table = '')
    {
        echo static::get_typoscript($typoscriptObjectPath, $data, $currentValueKey, $table);
    }

    /**
     * @param int $colPos
     * @return array
     */
    public static function get_content(int $colPos = 0)
    {
        return Common::context()->contentObjectRenderer->getRecords('tt_content', [
            'where' => 'colPos = ' . $colPos
        ]);
    }

    /**
     * Renders all content elements with $colPos using files named after the CType inside the given $folder.
     * E.g:
     * Given the function call: <?= get_render_content('ContentElements', 0, ['main' => true]); ?>
     * For ContentElement `textmedia`, this would render the template "ContentElements/Textmedia.php" with the
     *  arguments ['main' => true, 'data' => ...ContentElement record...]
     *
     * @param string $folder
     * @param int $colPos
     * @param array $arguments
     * @return string
     * @throws RenderingException
     */
    public static function get_render_content(string $folder, int $colPos = 0, array $arguments = [])
    {
        $output = '';
        $elements = static::get_content($colPos);

        foreach ($elements as $element) {
            $ctype = GeneralUtility::underscoredToUpperCamelCase($element['CType']);
            $template = $folder . '/' . $ctype;
            $templateArguments = array_merge(['data' => $element], $arguments);

            $output .= Common::get_template($template, $templateArguments);
        }

        return $output;
    }

    /**
     * @param string $folder
     * @param int $colPos
     * @param array $arguments
     * @throws RenderingException
     */
    public static function render_content(string $folder, int $colPos = 0, array $arguments = [])
    {
        echo static::get_render_content($folder, $colPos, $arguments);
    }

    /**
     * Calls a DataProcessor defined in TypoScript
     *
     * @param string $typoscriptConfigPath
     * @return void side effects the variables
     * @throws HelperException
     */
    public static function data_processor(string $typoscriptConfigPath)
    {
        list($processorName, $processorConfiguration) = static::typoscript_at($typoscriptConfigPath);

        if (!in_array(DataProcessorInterface::class, class_implements($processorName), true)) {
            throw new \UnexpectedValueException(
                'Processor with class name "' . $processorName . '" ' .
                'must implement interface "' . DataProcessorInterface::class . '"',
                1565256939
            );
        }

        $context = Common::context();
        /** @var DataProcessorInterface $processor */
        $processor = GeneralUtility::makeInstance($processorName);

        $context->variables = $processor->process(
            $context->contentObjectRenderer,
            $context->contentObjectConfiguration,
            $processorConfiguration,
            $context->variables
        );
    }

    /**
     * Returns a menu configuration
     *
     * @param string $menuProcessorConfigPath additional configuration passed to the MenuProcessor
     * @return array
     * @throws HelperException
     */
    public static function menu(string $menuProcessorConfigPath = '')
    {
        $context = Common::context();

        $configuration = $menuProcessorConfigPath
            ? static::typoscript_at($menuProcessorConfigPath)[1]
            : [];

        // make sure the menu is always saved to the "menu" variable so we can extract it to return only the menu
        $configuration['as'] = 'menu';
        if (isset($configuration['as.'])) {
            unset($configuration['as.']);
        }

        /** @var MenuProcessor $menuProcessor */
        $menuProcessor = GeneralUtility::makeInstance(MenuProcessor::class);

        $variables = $menuProcessor->process(
            $context->contentObjectRenderer,
            $context->contentObjectConfiguration,
            $configuration,
            []
        );

        return $variables['menu'];
    }
}
