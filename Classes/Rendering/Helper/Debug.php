<?php
namespace Jdoubleu\Phptemplates\Rendering\Helper;

use Jdoubleu\Phptemplates\Rendering\Exception\HelperException;
use Jdoubleu\Phptemplates\Rendering\RenderingContext;
use Jdoubleu\Phptemplates\Rendering\TemplateContext;

/**
 * Provides general helper and shorthand functions which can be used in PHPTemplates.
 */
final class Debug {
    /**
     * Dumps the context of the current template
     */
    public static function dump_template_context()
    {
        $context = Common::context();

        var_dump([
            'template_stack' => $context->templateContextStack,
            'template_context' => $context->currentTemplateContext,
            'arguments' => $context->currentTemplateContext->arguments,
            'variables' => $context->variables
        ]);
    }

    /**
     * Prints current template context information to JavaScript console
     *
     * @param RenderingContext $context
     * @param TemplateContext $templateContext
     * @throws HelperException
     */
    public static function debug_template(
        RenderingContext $context,
        TemplateContext $templateContext
    ) {
        $jsValue = json_encode([
            'template' => $templateContext->templateFile,
            'arguments' => $templateContext->arguments,
            'RenderingContext' => $context,
            'TemplateContext' => $templateContext
        ],  JSON_FORCE_OBJECT);

        if ($jsValue === false) {
            throw new HelperException(
                sprintf('Could not debug template "%s". Reason: %s', $templateContext->templateFile, json_last_error_msg()),
                1565265563
            );
        }

        echo '
        <script type="text/javascript">
        console.log(' . $jsValue . ');
        </script>
        ';
    }
}
