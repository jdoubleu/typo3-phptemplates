<?php
namespace Jdoubleu\Phptemplates\Rendering\Helper;

use Jdoubleu\Phptemplates\Rendering\DefaultView;
use Jdoubleu\Phptemplates\Rendering\Exception\RenderingException;
use Jdoubleu\Phptemplates\Rendering\RenderingContext;

/**
 * Provides general helper and shorthand functions which can be used in PHPTemplates.
 */
final class Common {
    /**
     * Returns the current rendering context
     * @return RenderingContext
     */
    public static function context()
    {
        return RenderingContext::getInstance(null);
    }

    /**
     * Returns the current view which renders this template
     * @return DefaultView
     */
    public static function view()
    {
        return RenderingContext::getInstance(null)->view;
    }

    /**
     * Accesses the arguments of the current template
     * @param string $name
     * @return mixed
     */
    public static function argument(string $name)
    {
        return static::context()->currentTemplateContext->arguments[$name];
    }

    /**
     * Short-hand for `argument('content')`
     * @return string
     */
    public static function content()
    {
        return static::argument('content');
    }

    /**
     * Accesses the variables of the rendering context
     * @param string $name
     * @return mixed
     */
    public static function variable(string $name)
    {
        return static::view()->renderingContext->variables[$name];
    }

    /**
     * @param string $name of the template
     * @param array $arguments passed to the template
     * @return void rendered template output
     * @throws RenderingException
     *
     * @example `<?php template('my_template', ['arg1' => 42]); ?>`
     */
    public static function template(string $name, array $arguments = [])
    {
        static::view()->renderTemplate($name, $arguments);
    }

    /**
     * @param string $name of the template
     * @param array $arguments passed to the template
     * @return false|string rendered template output
     * @throws RenderingException
     */
    public static function get_template(string $name, array $arguments = [])
    {
        return static::view()->getTemplate($name, $arguments);
    }

    /**
     * @param string $name of the template
     * @param array $arguments passed to the template
     * @return void rendered template output
     *
     * @example `
     * <?php start_template('my_template', ['arg1' => 42]); ?>
     * <p>HTML content</p>
     * <?php end_template(); ?>
     * `
     */
    public static function start_template(string $name, array $arguments = [])
    {
        static::context()->outputCaptureStack->push(function ($content, $output) use ($name, $arguments) {
            $arguments['content'] = $content;

            $renderFn = 'renderTemplate';
            if ($output) {
                $renderFn = 'getTemplate';
            }

            return static::view()->$renderFn($name, $arguments);
        });
    }

    /**
     * Echoes the rendered template output with the captured content
     * @return void rendered template output
     */
    public static function end_template()
    {
        static::context()->outputCaptureStack->pop(false);
    }

    /**
     * Returns the rendered template output with the captured content
     * @return string rendered template output
     */
    public static function get_end_template()
    {
        return static::context()->outputCaptureStack->pop(true);
    }

    /**
     * Prints information about the current template
     */
    public static function debug_template()
    {
        Debug::dump_template_context();
    }
}
