<?php
/**
 * (Common) PHPTEMPLATE function aliases for quick access.
 */

namespace Jdoubleu\Phptemplates\Rendering\Context;

use Jdoubleu\Phptemplates\Rendering\Helper\Common;

/**
 * @see Common::context
 */
function context()
{
    return Common::context();
}

/**
 * @see Common::view
 */
function view()
{
    return Common::view();
}

/**
 * @see Common::argument
 */
function argument(string $name)
{
    return Common::argument($name);
}

/**
 * @see Common::content
 */
function content()
{
    return Common::content();
}

/**
 * @see Common::variable
 */
function variable(string $name)
{
    return Common::variable($name);
}

/**
 * @see Common::template
 */
function template(string $name, array $arguments = [])
{
    Common::template($name, $arguments);
}

/**
 * @see Common::get_template
 */
function get_template(string $name, array $arguments = [])
{
    return Common::get_template($name, $arguments);
}

/**
 * @see Common::start_template
 */
function start_template(string $name, array $arguments = [])
{
    Common::start_template($name, $arguments);
}

/**
 * @see Common::end_template
 */
function end_template()
{
    Common::end_template();
}

/**
 * @see Common::get_end_template
 */
function get_end_template()
{
    return Common::get_end_template();
}

/**
 * @see Common::debug_template
 */
function debug_template()
{
    Common::debug_template();
}
