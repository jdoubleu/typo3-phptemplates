<?php
$EM_CONF[$_EXTKEY] = [
    'title' => 'PHP Templates',
    'description' => 'Write templates in plain PHP and HTML.',
    'category' => 'fe',
    'author' => 'Joshua Westerheide',
    'author_email' => 'dev@jdoubleu.de',
    'author_company' => '',
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '1.0.0',
    'constraints' => [
        'depends' => [
            'core' => '9.5.6',
            'extbase' => '9.5.6'
        ],
        'conflicts' => [],
        'suggests' => []
    ],
];
