<?php
/**
 * Render the main menu
 */

namespace Jdoubleu\Phptemplates\Rendering\Context;

$menu = argument('menu');
?>

<nav>
    <ul>
        <?php foreach ($menu as $item) : ?>
            <li class="menu-item <?= $item['active'] ? ' active' : '' ?>">
                <a href="<?= $item['link'] ?>"><?= $item['title'] ?></a>
                <?php if ($item['children']) : ?>
                    <?php template('_menu', ['menu' => $item['children']]); ?>
                <?php endif; ?>
            </li>
        <?php endforeach; ?>
    </ul>
</nav>
