<?php
/**
 * Renders a headline
 * @param int $level define the level of the heading (e.g. 3 => h3)
 * @param string $content the text-content of the headline
 */

namespace Jdoubleu\Phptemplates\Rendering\Context;

?>
<h<?= argument('level') ?>>
<?= content(); ?>
</h<?= argument('level') ?>>
