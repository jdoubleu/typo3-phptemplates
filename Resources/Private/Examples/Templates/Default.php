<?php
/** Default template of the page rendering */

namespace Jdoubleu\Phptemplates\Rendering\Context;
use Jdoubleu\Phptemplates\Rendering\Helper\TYPO3\TYPO3;
use TYPO3\CMS\Core\Resource\FileReference;

$mainmenu = TYPO3::menu('lib.phptemplates.typoscript.page.menu');

TYPO3::data_processor('lib.phptemplates.typoscript.page.filesProcessor');

$files = variable('files');

?>
<?php start_template('Helper/title', ['level' => 1]); ?>
PHPTEMPLATE
<?= get_end_template(); ?>

<?php template('_menu', ['menu' => $mainmenu]); ?>

<?php if ($files) : ?>
<section>
    <h3>List of files</h3>
    <ul>
        <?php foreach ($files as $file) : /** @var FileReference $file */?>
            <li><?= $file->getName() ?> <i>(<?= $file->getSize() ?> bytes)</i></li>
        <?php endforeach; ?>
    </ul>
</section>
<?php endif; ?>

<p>
    This content is rendered using the <strong>PHPTEMPLATE</strong> template engine.
    You can view its content in the <code><?= context()->currentTemplateContext->templateFile; ?></code> file.
</p>
<p>The page content will be rendered below, using a template.</p>
<hr/>
<?php template('_content'); ?>

<?php foreach(TYPO3::get_content() as $row): ?>
    <article>
        <header>
            <h2><?= $row['header'] ?></h2>
        </header>

        <?= $row['bodytext'] ?>
    </article>
<?php endforeach; ?>

<hr/>
<h3>Auto CONTENT</h3>
<?php TYPO3::render_content('ContentElement'); ?>
