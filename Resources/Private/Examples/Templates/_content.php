<?php
/**
 * Render child content
 */

use Jdoubleu\Phptemplates\Rendering\Helper\Common;
use Jdoubleu\Phptemplates\Rendering\Helper\TYPO3\TYPO3;

?>

<?php Common::start_template('Helper/title', ['level' => 2]); ?>
CONTENT
<?php Common::end_template(); ?>

<?php TYPO3::typoscript('styles.content.get', null); ?>
