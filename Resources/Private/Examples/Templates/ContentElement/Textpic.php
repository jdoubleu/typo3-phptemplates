<?php
/**
 * Render "images" ContentElement
 */

use Jdoubleu\Phptemplates\Rendering\Helper\Common;

$data = Common::argument('data');

?>
<article>
    <header><?= $data['header'] ?></header>
    <?php if ($data['media']): ?>
        <figure>
            <img src="<?= $data['media'] ?>"/>
        </figure>
    <?php endif; ?>
    <section>
        <?= $data['bodytext'] ?>
    </section>
</article>
