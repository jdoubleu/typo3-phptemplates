# TYPO3 PHPTEMPLATES
`PHPTEMPLATES` is a [TYPO3](https://typo3.org/) extension which implements a simple template rendering engine based on plain PHP files.
It can be used alongside the well-known [Fluid template engine](https://typo3.org/fluid/) or as a replacement for it.

## Installation
At the moment the only way to install this extension is through [Composer](https://getcomposer.org/):
```sh
composer require jdoubleu/typo3-phptemplates
```

## State of Development
**WARNING:**
This extension/template engine is still under development. The API may be subject to breaking changes in the future.
As this is an experimental template engine the internals may also change. If you have any suggestions, critic or use cases
please feel free to share them.

## Motivation
Following some criticism about the complexity of [Fluid templates](https://docs.typo3.org/m/typo3/guide-extbasefluid/master/en-us/Fluid/Index.html) 
and limitations by the template engine itself, I wanted to implement a simple-as-possible template engine while
still providing enough features for general page rendering. In fact, `PHPTEMPLATES` only implement a template resolving and
overriding mechanism as well as a minimal and easy to learn API to interact with certain TYPO3 internals (e.g. TypoScript, ContentElements, etc.).

This template engine is meant to be opinionated and thought to work only a specific way, although it is possible to 
extend the internals and behaviour.

## Features
* **Easy to learn:** You only need to know PHP itself, a minimal (~5 LOC) initial configuration and a small and simple API
* **Fast:** Since we are using PHP files already we don't need to render any template files back to PHP. 
    However this might only affect development time, because templates are cached in production.
* **Flexibility:** You can write any PHP code and logic inside the templates and don't need to know how to write [`ViewHelper`](https://docs.typo3.org/m/typo3/book-extbasefluid/master/en-us/8-Fluid/8-developing-a-custom-viewhelper.html) for example.
* **Easy debuggable:** You can either use PHP's built-in `echo`, `print`, `var_dump`, etc. functions or use [XDebug](https://xdebug.org/)
    and set breakpoints directly in your template files.

Given the features, the developer is still responsible for writing clean code, meaning to not blow up templates and 
separate business logic from presentation. 

## Documentation
*TODO*

### Examples
As long as there is no official documentation, follow the examples in [`Resources/Private/Examples`](Resources/Private/Examples).

## License
[GPLv3](LICENSE)
